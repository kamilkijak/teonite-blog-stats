export const FETCH_AUTHOR_STATS = 'FETCH_AUTHOR_STATS';
export const POPULATE_AUTHOR_STATS = 'POPULATE_AUTHOR_STATS';


export const fetchAuthorStats = (username) => ({
    type: FETCH_AUTHOR_STATS,
    username,
});

export const populateAuthorStats = (username, stats) => ({
    type: POPULATE_AUTHOR_STATS,
    username,
    stats,
});
