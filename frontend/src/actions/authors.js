export const FETCH_AUTHORS = 'FETCH_AUTHORS';
export const POPULATE_AUTHORS = 'POPULATE_AUTHORS';
export const SELECT_AUTHORS = 'SELECT_AUTHORS';


export const fetchAuthors = () => ({
    type: FETCH_AUTHORS,
});

export const selectAuthors = (selectedAuthors) => ({
    type: SELECT_AUTHORS,
    selectedAuthors,
});