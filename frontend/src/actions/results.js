export const CALCULATE_RESULTS = 'CALCULATE_RESULTS';


export const calculateResults = (statsToCalculate) => ({
    type: CALCULATE_RESULTS,
    statsToCalculate,
});
