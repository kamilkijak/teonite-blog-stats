export const FETCH_GLOBAL_STATS = 'FETCH_GLOBAL_STATS';
export const POPULATE_GLOBAL_STATS = 'POPULATE_GLOBAL_STATS';


export const fetchGlobalStats = () => ({
    type: FETCH_GLOBAL_STATS,
});

export const populateGlobalStats = (stats) => ({
    type: POPULATE_GLOBAL_STATS,
    stats,
});
