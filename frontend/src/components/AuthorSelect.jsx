import PropTypes from 'prop-types';
import React from 'react';
import Select from 'react-select';
import { connect } from 'react-redux';
import { fetchAuthors, selectAuthors } from '../actions';


export const optionForAll = {
    value: 'all',
    label: 'All',
};  // option in multi-select for choosing all authors

export class AuthorSelect extends React.Component {

    constructor(props) {
        super(props);

        this.fetchAuthors = this.fetchAuthors.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.getOptions = this.getOptions.bind(this);
    }


    componentDidMount() {
        this.fetchAuthors();
    }

    fetchAuthors() {
        const authorsFetched = this.props.fetchAuthors();

        this.setState({
            authors: authorsFetched
        });
    }

    getOptions() {
        const options = Object.keys(this.props.authors)
            .map(username => {
                return {value: username, label: this.props.authors[username]};
            });

        options.push(optionForAll);
        return options;
    }

    handleChange(selectedAuthors) {
        const newSelection = selectedAuthors.filter(auth => !this.props.selectedAuthors.includes(auth))[0];
        const isAllAlreadySelected = this.props.selectedAuthors.some(a => a.value === optionForAll.value);

        if (newSelection && newSelection.value === optionForAll.value) {
            // when 'ALL' is selected, remove all other selected options
            this.props.selectAuthors([optionForAll]);
        }
        else if (newSelection && isAllAlreadySelected) {
            // when specific author is chosen, remove 'ALL' from selected options
            const authorsWithoutAll = selectedAuthors.filter(a => a.value !== optionForAll.value);
            this.props.selectAuthors(authorsWithoutAll);
        }
        else {
            this.props.selectAuthors(selectedAuthors);
        }
    }

    render() {
        return (
            <Select
                value={this.props.selectedAuthors}
                onChange={this.handleChange}
                options={this.getOptions()}
                isMulti
                className='basic-multi-select'
                name='authors'
            />
        );
    }
}

AuthorSelect.propTypes = {
    authors: PropTypes.objectOf(PropTypes.string).isRequired,
    selectedAuthors: PropTypes.array.isRequired,
    fetchAuthors: PropTypes.func.isRequired,
    selectAuthors: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
    authors: state.authors.all,
    selectedAuthors: state.authors.selected,
});

const mapDispatchToProps = {
    fetchAuthors,
    selectAuthors,
};

export default connect(mapStateToProps, mapDispatchToProps)(AuthorSelect);
