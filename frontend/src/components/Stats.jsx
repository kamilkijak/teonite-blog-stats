import React from 'react';
import AuthorSelect from './AuthorSelect.jsx';
import ResultsTable from './ResultsTable.jsx';

const Stats = () => (
    <div>
        <h1>Teonite Stats</h1>
        <AuthorSelect/>
        <ResultsTable/>
    </div>
);

export default Stats;