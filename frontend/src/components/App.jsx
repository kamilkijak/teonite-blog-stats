import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Stats from './Stats.jsx';


const App = () => (
    <Router>
        <div>
            <Route path='/stats' component={Stats} />
        </div>
    </Router>
);

export default App;