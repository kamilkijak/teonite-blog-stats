import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';


class ResultsTable extends React.Component {

    constructor(props) {
        super(props);

        this.RECORDS_RENDERED = 10;
    }

    renderRows() {
        let sortedResults = Object.keys(this.props.results)
            .sort((a, b) => this.props.results[b] - this.props.results[a]);
        sortedResults = sortedResults.slice(0, this.RECORDS_RENDERED);
        return sortedResults.map(result => {
            return (
                <tr key={result} style={{border: '1px solid black'}}>
                    <td>{result}</td>
                    <td>{this.props.results[result]}</td>
                </tr>
            );
        });
    }

    render() {
        return (
            <div style={{textAlign: 'center', marginTop: '15px'}}>
                <table style={{'border': '1px solid black', 'margin': 'auto'}}>
                    <thead>
                        <tr>
                            <th>Word</th>
                            <th>Occurrences</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.renderRows()}
                    </tbody>
                </table>
            </div>
        );
    }
}

ResultsTable.propTypes = {
    results: PropTypes.objectOf(PropTypes.number).isRequired,
};

const mapStateToProps = state => ({
    results: state.results,
});

export default connect(mapStateToProps)(ResultsTable);
