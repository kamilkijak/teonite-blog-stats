import { call, put, takeEvery } from 'redux-saga/effects';
import { FETCH_AUTHOR_STATS, POPULATE_AUTHOR_STATS } from '../actions';


export function* fetchAuthorStats(action) {
    try {
        let fetchedStats = yield call(get, action.username);

        yield put({
            type: POPULATE_AUTHOR_STATS,
            username: action.username,
            stats: fetchedStats,
        });
    } catch (error) {
        console.error(error);
    }
}

export function* watchAuthorStats() {
    yield takeEvery(FETCH_AUTHOR_STATS, fetchAuthorStats);
}

function get(username) {
    return fetch(`http://localhost:8080/stats/${username}/`)
        .then(res => res.json())
        .then(data => data);
}