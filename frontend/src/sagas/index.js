import { all } from 'redux-saga/effects';
import { watchAuthors } from './authors';
import { watchAuthorStats } from './authorStats';
import { watchGlobalStats } from './globalStats';
import { watchResults } from './results';


export default function* rootSaga() {
    yield all([
        watchAuthors(),
        watchAuthorStats(),
        watchGlobalStats(),
        watchResults(),
    ]);
}
