import { call, put, takeEvery } from 'redux-saga/effects';
import { FETCH_AUTHORS, POPULATE_AUTHORS } from '../actions';


export function* fetchAuthors() {
    try {
        let authors = yield call(get);
        yield put({type: POPULATE_AUTHORS, authors: authors});
    } catch (error) {
        console.error(error);
    }
}

export function* watchAuthors() {
    yield takeEvery(FETCH_AUTHORS, fetchAuthors);
}

function get() {
    return fetch('http://localhost:8080/authors/')
        .then(res => res.json())
        .then(data => data);
}
