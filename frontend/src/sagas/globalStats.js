import { call, put, takeEvery } from 'redux-saga/effects';
import { FETCH_GLOBAL_STATS, POPULATE_GLOBAL_STATS } from '../actions';


export function* fetchGlobalStats() {
    try {
        let fetchedStats = yield call(get);
        yield put({
            type: POPULATE_GLOBAL_STATS,
            stats: fetchedStats,
        });
    } catch (error) {
        console.error(error);
    }
}

export function* watchGlobalStats() {
    yield takeEvery(FETCH_GLOBAL_STATS, fetchGlobalStats);
}

function get() {
    return fetch('http://localhost:8080/stats/')
        .then(res => res.json())
        .then(data => data);
}