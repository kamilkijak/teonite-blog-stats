import { all, call, put, select, takeLatest } from 'redux-saga/effects';
import { CALCULATE_RESULTS, SELECT_AUTHORS } from '../actions';
import { optionForAll } from '../components/AuthorSelect.jsx';
import { fetchAuthorStats } from './authorStats';
import { fetchGlobalStats } from './globalStats';


export const getStatsOfSelectedAuthors = (state) => {
    const selectedAuthorsUsernames = state.authors.selected.map(auth => auth.value);
    return selectedAuthorsUsernames
        .map(username => state.authorStats[username])
        .filter(stats => stats !== undefined);
};

export const filterAuthorsToBeFetched = (state, authors) => {
    return authors
        .filter(author => author !== optionForAll)
        .filter(author => state.authorStats[author.value] === undefined);
};

export const isFetchingOfGlobalStatsRequired = (state, authors) => {
    return authors.some(author => author.value === optionForAll.value)
        && Object.keys(state.globalStats).length === 0;
};

export const checkIfOptionAllIsSelected = (state) => {
    const selectedAuthors = state.authors.selected;
    return selectedAuthors && selectedAuthors.some(a => a.value === optionForAll.value);
};

export function* updateResults(action) {
    const isAllSelected = yield select(checkIfOptionAllIsSelected);

    if (isAllSelected) {
        const shouldFetchGlobalStats = yield select(isFetchingOfGlobalStatsRequired, action.selectedAuthors);
        if (shouldFetchGlobalStats) {
            yield call(fetchGlobalStats);
        }
        yield call(updateResultsWithGlobalStats);
    }
    else {
        const missingAuthors = yield select(filterAuthorsToBeFetched, action.selectedAuthors);

        // fetch stats for all selected authors that are not fetched yet
        yield all(missingAuthors.map(author => call(fetchAuthorStats, {username: author.value})));

        const statsToCalculate = yield select(getStatsOfSelectedAuthors);
        yield put({
            type: CALCULATE_RESULTS,
            statsToCalculate: statsToCalculate,
        });
    }
}

export const getGlobalStats = (state) => {
    return state.globalStats;
};

export function* updateResultsWithGlobalStats() {
    const globalStats = yield select(getGlobalStats);
    yield put({
        type: CALCULATE_RESULTS,
        statsToCalculate: [globalStats],
    });
}

export function* watchResults() {
    yield takeLatest(SELECT_AUTHORS, updateResults);
}