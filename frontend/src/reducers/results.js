/* eslint no-case-declarations: 0 */
import { CALCULATE_RESULTS } from '../actions';


const results = (state = {}, action) => {
    switch (action.type) {

        case CALCULATE_RESULTS:
            let results = {};

            action.statsToCalculate.forEach(stats => {
                Object.keys(stats).map(word => {
                    results[word] = (results[word] || 0) + stats[word];
                });
            });

            return {
                ...results,
            };

        default:
            return state;
    }
};

export { results };