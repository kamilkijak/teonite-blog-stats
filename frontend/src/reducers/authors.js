import { SELECT_AUTHORS ,POPULATE_AUTHORS } from '../actions';


const initialState = {
    all: {},
    selected: [],
};

const authors = (state = initialState, action) => {
    switch (action.type) {

        case POPULATE_AUTHORS:
            return {
                ...state,
                all: action.authors,
            };

        case SELECT_AUTHORS:
            return {
                ...state,
                selected: action.selectedAuthors,
            };

        default:
            return state;
    }
};

export { authors };