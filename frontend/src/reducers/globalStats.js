import { POPULATE_GLOBAL_STATS } from '../actions';


const globalStats = (state = {}, action) => {
    switch (action.type) {

        case POPULATE_GLOBAL_STATS:
            return {
                ...action.stats,
            };

        default:
            return state;
    }
};

export { globalStats };