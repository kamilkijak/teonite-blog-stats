import { POPULATE_AUTHOR_STATS } from '../actions';


const authorStats = (state = {}, action) => {
    switch (action.type) {

        case POPULATE_AUTHOR_STATS:
            return {
                ...state,
                [action.username]: action.stats,
            };

        default:
            return state;
    }
};

export { authorStats };