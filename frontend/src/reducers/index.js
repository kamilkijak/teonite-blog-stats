import { combineReducers } from 'redux';
import { authors } from './authors';
import { authorStats } from './authorStats';
import { globalStats } from './globalStats';
import { results } from './results';

const reducer = combineReducers({
    authors,
    authorStats,
    globalStats,
    results,
});

export { reducer };
