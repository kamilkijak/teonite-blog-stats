import { authors } from '../../reducers/authors';


describe('authors reducer', () => {

    it('should handle initial state', () => {
        expect(
            authors(undefined, {})
        ).toEqual({
            all: {},
            selected: [],
        });
    });

});
