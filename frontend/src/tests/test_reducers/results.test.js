import { results } from '../../reducers/results';
import { CALCULATE_RESULTS } from '../../actions/index';


describe('results reducer', () => {

    it('should handle initial state', () => {
        expect(
            results(undefined, {})
        ).toEqual({});
    });

    it('should handle CALCULATE_RESULTS', () => {
        const statsToCalculate = [
            {'a': 1, 'b': 1, 'c': 1},
            {'d': 2, 'e': 2, 'f': 2},
            {'a': 10, 'b': 20, 'x': 100},
        ];
        
        const expectedResults = {
            'a': 11,
            'b': 21,
            'c': 1,
            'd': 2,
            'e': 2,
            'f': 2,
            'x': 100,
        };

        expect(
            results({}, {
                type: CALCULATE_RESULTS,
                statsToCalculate,
            })
        ).toEqual({
            ...expectedResults
        });
    });

});
