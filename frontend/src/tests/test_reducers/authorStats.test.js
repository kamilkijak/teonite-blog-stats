import { authorStats } from '../../reducers/authorStats';
import { POPULATE_AUTHOR_STATS } from '../../actions/index';


describe('authorStats reducer', () => {

    it('should handle initial state', () => {
        expect(
            authorStats(undefined, {})
        ).toEqual({});
    });

    it('should handle POPULATE_AUTHOR_STATS', () => {
        const username = 'test';
        const stats = {'a': 1, 'b': 2, 'c': 3};

        expect(
            authorStats({}, {
                type: POPULATE_AUTHOR_STATS,
                username: username,
                stats: stats,
            })
        ).toEqual({
            [username]: stats
        });
    });

});
