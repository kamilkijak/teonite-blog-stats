import { globalStats } from '../../reducers/globalStats';
import { POPULATE_GLOBAL_STATS } from '../../actions/index';


describe('authorStats reducer', () => {

    it('should handle initial state', () => {
        expect(
            globalStats(undefined, {})
        ).toEqual({});
    });

    it('should handle POPULATE_GLOBAL_STATS', () => {
        const stats = {'a': 1, 'b': 2, 'c': 3};

        expect(
            globalStats({}, {
                type: POPULATE_GLOBAL_STATS,
                stats: stats,
            })
        ).toEqual({
            ...stats
        });
    });

});
