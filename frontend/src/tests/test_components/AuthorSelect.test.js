import React from 'react';
import { shallow } from 'enzyme';
import { AuthorSelect, optionForAll } from '../../components/AuthorSelect.jsx';  // import component, not container


describe('AuthorSelect component', () => {

    let mockedFetchAuthors, mockedSelectAuthors, props, testAuthor;

    beforeEach(() => {
        mockedSelectAuthors = jest.fn();
        mockedFetchAuthors = jest.fn();

        testAuthor = {value: 'test author', label: 'Test Author'};

        props = {
            authors: {
                [testAuthor.value]: testAuthor.label,
                [optionForAll.value] : optionForAll.label
            },
            fetchAuthors: mockedFetchAuthors,
            selectedAuthors: [],
            selectAuthors: mockedSelectAuthors,
        };
    });

    it('handleChange should handle empty array', () => {
        const wrapper = shallow(<AuthorSelect {...props}/>);

        wrapper.instance().handleChange([]);
        expect(mockedSelectAuthors.mock.calls.length).toBe(1);
        expect(mockedSelectAuthors.mock.calls[0][0]).toEqual([]);
    });

    it('handleChange should handle selection of ALL when there is already an author chosen', () => {
        props.selectedAuthors = [testAuthor, ];
        const wrapper = shallow(<AuthorSelect {...props}/>);

        wrapper.instance().handleChange([optionForAll, ]);
        expect(mockedSelectAuthors.mock.calls.length).toBe(1);
        expect(mockedSelectAuthors.mock.calls[0][0]).toEqual([optionForAll, ]);
    });

    it('handleChange should handle selection of specific author when there is ALL already chosen', () => {
        props.selectedAuthors = [optionForAll, ];

        const wrapper = shallow(<AuthorSelect {...props}/>);

        wrapper.instance().handleChange([testAuthor, ]);
        expect(mockedSelectAuthors.mock.calls.length).toBe(1);

        // option 'ALL' should be removed when specific author is chosen
        expect(mockedSelectAuthors.mock.calls[0][0]).toEqual([testAuthor, ]);
    });

    it('handleChange should handle selection of specific author when there is another author already chosen', () => {
        props.selectedAuthors = [testAuthor, ];

        const wrapper = shallow(<AuthorSelect {...props}/>);

        const newAuthor = {value: 'new author', label: 'New Author'};

        wrapper.instance().handleChange([testAuthor, newAuthor, ]);
        expect(mockedSelectAuthors.mock.calls.length).toBe(1);

        expect(mockedSelectAuthors.mock.calls[0][0]).toEqual([testAuthor, newAuthor, ]);
    });

});
