# Teonite blog statistics
[![pipeline status](https://gitlab.com/kamilkijak/teonite-blog-stats/badges/master/pipeline.svg)](https://gitlab.com/kamilkijak/teonite-blog-stats/commits/master)
[![coverage report](https://gitlab.com/kamilkijak/teonite-blog-stats/badges/master/coverage.svg)](https://gitlab.com/kamilkijak/teonite-blog-stats/commits/master)

This application shows statistics of words used in articles on [Teonite blog](https://teonite.com/blog).

## Running

Run app using docker-compose
```
docker-compose up
```
now API should be up and running on `localhost:8080` and client on `localhost:3000`.

Run `backend/build.sh` **from the host machine** 
to scrape the blog and to save statistics into the database.
```
./backend/build.sh
```
_Note: be patient, it needs some time to scrape all posts._

## API Endpoints (localhost:8080)

- `/stats/` - most popular words from the blog
- `/stats/<author>` - most popular words per author
- `/authors/` - list of authors


## Client Endpoints (localhost:3000)
- `/stats/` - an interface for showing stats