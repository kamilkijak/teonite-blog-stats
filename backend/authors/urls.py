from django.urls import path

from authors.views import AuthorListView

urlpatterns = [
    path('', AuthorListView.as_view(), name='author-list'),
]
