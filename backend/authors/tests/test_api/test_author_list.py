from model_mommy import mommy
from parameterized import parameterized
from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from authors.models import Author


class AuthorListViewTestCase(APITestCase):

    def test_get_all_authors_when_db_is_empty(self):
        Author.objects.all().delete()

        response = self.client.get(reverse('author-list'))

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {})

    @parameterized.expand([
        (1, ),
        (2, ),
        (500, ),
    ])
    def test_get_all_authors(self, number_of_authors):
        authors = mommy.make(
            Author,
            _quantity=number_of_authors,
        )

        response = self.client.get(reverse('author-list'))

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        expected_data = {author.username: author.full_name for author in authors}
        self.assertDictEqual(response.data, expected_data)
