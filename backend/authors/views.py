from rest_framework.response import Response
from rest_framework.views import APIView

from authors.models import Author


class AuthorListView(APIView):
    http_method_names = ['get', ]

    def get(self, request):
        response_data = {author.username: author.full_name for author in Author.objects.all()}
        return Response(data=response_data)
