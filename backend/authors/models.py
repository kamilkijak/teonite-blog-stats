from django.db import models


class Author(models.Model):
    username = models.CharField(max_length=255, unique=True)
    full_name = models.CharField(max_length=255)

    def __repr__(self):
        return self.username

    @classmethod
    def full_name_to_username(cls, full_name: str) -> str:
        return full_name.replace(' ', '').lower()
