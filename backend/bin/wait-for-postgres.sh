#!/bin/sh
set -e

host="$1"
port="$2"

until pg_isready -h ${host} -p ${port}
do
    sleep 1
done