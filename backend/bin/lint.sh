#!/bin/sh
set -e

pip install -r requirements-test.txt
echo "=============================== checking isort ================================="
isort -rc -c -df
echo "=============================== checking pylint ================================"
pylint --load-plugins pylint_django teonite_blog_stats stats && echo "pylint is happy" || exit $?
