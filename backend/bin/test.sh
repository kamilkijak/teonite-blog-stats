#!/bin/sh
set -e

pip install -r requirements-test.txt

coverage run --source='.' manage.py test
coverage report
