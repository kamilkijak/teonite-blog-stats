from django.core.management import BaseCommand

from stats.importers import TeoniteBlogPostsImporter


class Command(BaseCommand):
    help = 'Parse Teonite blog, get posts and save them as statistics into the database.'

    def handle(self, *args, **options):
        importer = TeoniteBlogPostsImporter()
        self.stdout.write(self.style.SUCCESS('Starting importing stats...'))
        importer.run()
        self.stdout.write(self.style.SUCCESS('Successfully imported stats'))
