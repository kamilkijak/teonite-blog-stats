# pylint: disable=invalid-name
from operator import attrgetter

from django.db import IntegrityError
from django.test import TestCase
from model_mommy import mommy

from authors.models import Author
from stats.models import Post, WordCounter
from stats.tests.utils import generate_random_string


class WordCounterTestCase(TestCase):

    def test_if_word_field_is_required(self):
        post = mommy.make(Post)

        with self.assertRaises(IntegrityError):
            WordCounter.objects.create(post=post, counter=1)

    def test_get_most_popular_words(self):
        limit = 10
        words = mommy.make(WordCounter, _quantity=100, _fill_optional=['word', ])

        expected_most_popular_words = sorted(
            words, key=attrgetter('counter'), reverse=True,
        )[:limit]

        most_popular_words = WordCounter.get_most_popular_words(limit)

        self.assertListEqual(
            list(most_popular_words),
            [{'word': wc.word, 'counter': wc.counter} for wc in expected_most_popular_words],
        )

    def test_get_author_most_popular_words(self):
        author = mommy.make(Author)
        post = mommy.make(Post, author=author)
        limit = 10
        words = mommy.make(WordCounter, post=post, _quantity=100, _fill_optional=['word', ])

        expected_author_most_popular_words = sorted(
            words, key=attrgetter('counter'), reverse=True,
        )[:limit]

        author_most_popular_words = WordCounter.get_author_most_popular_words(author, limit)

        self.assertListEqual(
            list(author_most_popular_words),
            [{'word': wc.word, 'counter': wc.counter} for wc in expected_author_most_popular_words],
        )

    def test_get_author_most_popular_words_when_same_word_occurs_in_different_posts(self):
        """
        Corner case spotted during manual tests.

        When specific word occurs in different posts of specific author, it should be visible in
        stats only once, with proper counter.

        Eg. for word 'the':
            * 1st post - 'the' occurs 10 times
            * 2nd post - 'the' occurs 3 times
        result should be [{'word': 'the', 'counter': 13}, ]
        """
        word = generate_random_string()
        author = mommy.make(Author)

        first_post = mommy.make(Post, author=author)
        second_post = mommy.make(Post, author=author)

        first_word_counter = WordCounter.objects.create(post=first_post, word=word, counter=10)
        second_word_counter = WordCounter.objects.create(post=second_post, word=word, counter=3)

        total_word_counter = first_word_counter.counter + second_word_counter.counter

        most_popular_words = WordCounter.get_author_most_popular_words(author, 10)

        self.assertEqual(
            len(most_popular_words), 1, msg='Only one word has been added for the author',
        )
        self.assertEqual(most_popular_words[0]['word'], word)
        self.assertEqual(most_popular_words[0]['counter'], total_word_counter)
