# pylint: disable=invalid-name

import random
from operator import attrgetter

from model_mommy import mommy
from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from authors.models import Author
from stats.models import Post, WordCounter
from stats.tests.utils import generate_random_string
from stats.views import AuthorStatsView


class AuthorStatsTestCase(APITestCase):

    def setUp(self):
        self.author = mommy.make(Author)
        self.author_posts = mommy.make(
            Post,
            author=self.author,
            _quantity=random.randint(2, 10),
        )
        self.author_words = []

        for _ in range(random.randint(2, 100)):
            self.author_words.append(
                mommy.make(
                    WordCounter,
                    post=random.choice(self.author_posts),
                    word=generate_random_string(),
                )
            )

    def test_get_stats_when_db_is_empty(self):
        WordCounter.objects.all().delete()  # make sure there are no words in DB
        response = self.client.get(
            reverse('author-stats', kwargs={'username': self.author.username})
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {})

    def test_get_stats_of_nonexistent_author(self):
        nonexistent_username = generate_random_string()
        response = self.client.get(
            reverse('author-stats', kwargs={'username': nonexistent_username})
        )

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_if_author_stats_are_returned_properly(self):
        most_popular_words = sorted(
            self.author_words, key=attrgetter('counter'), reverse=True,
        )[:AuthorStatsView.NUMBER_OF_WORDS]

        response = self.client.get(
            reverse('author-stats', kwargs={'username': self.author.username})
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        for word in most_popular_words:
            self.assertIn(word.word, response.data)
            self.assertEqual(response.data[word.word], word.counter)

    def test_if_only_limited_number_of_words_is_returned(self):
        current_limit = AuthorStatsView.NUMBER_OF_WORDS
        mommy.make(
            WordCounter,
            word=generate_random_string,
            post=self.author_posts[0],
            _quantity=current_limit + 100,  # create more records
        )

        response = self.client.get(
            reverse('author-stats', kwargs={'username': self.author.username})
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), current_limit)
