# pylint: disable=invalid-name

from collections import namedtuple

from model_mommy import mommy
from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from stats.models import WordCounter
from stats.tests.utils import generate_random_string
from stats.views import MostPopularWordsView

Word = namedtuple('Word', ['word', 'occurrences'])  # helper for creating test data


class MostPopularWordsTestCase(APITestCase):

    def test_get_stats_when_db_is_empty(self):
        WordCounter.objects.all().delete()  # make sure there are no words in DB
        response = self.client.get(reverse('most-popular-words'))

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {})

    def test_if_most_popular_words_are_returned_properly(self):
        popular_words = [
            Word(word=generate_random_string(), occurrences=1000),
            Word(word=generate_random_string(), occurrences=900),
            Word(word=generate_random_string(), occurrences=800),
        ]

        for word in popular_words:
            mommy.make(WordCounter, word=word.word, counter=word.occurrences)

        response = self.client.get(reverse('most-popular-words'))

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        for word in popular_words:
            self.assertIn(word.word, response.data)
            self.assertEqual(response.data[word.word], word.occurrences)

    def test_if_only_limited_number_of_words_is_returned(self):
        current_limit = MostPopularWordsView.NUMBER_OF_WORDS
        mommy.make(
            WordCounter,
            word=generate_random_string,
            _quantity=current_limit + 100,  # create more records
        )

        response = self.client.get(reverse('most-popular-words'))

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), current_limit)
