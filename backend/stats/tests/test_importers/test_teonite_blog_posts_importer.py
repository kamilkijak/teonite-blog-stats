# pylint: disable=protected-access

from unittest.mock import patch

from django.test import TestCase
from model_mommy import mommy
from parameterized import parameterized

from authors.models import Author
from stats.importers import TeoniteBlogPostsImporter
from stats.models import Post, WordCounter
from stats.tests.utils import generate_random_string
from stats.web_scrapers import PostData, TeoniteBlogWebScraper


class TeoniteBlogPostsImporterTestCase(TestCase):

    def setUp(self):
        self.importer = TeoniteBlogPostsImporter()

    def assert_post_imported_properly(self, post_data, number_of_words_in_text):
        self.assertTrue(
            Author.objects.filter(
                full_name=post_data.author,
                username=Author.full_name_to_username(post_data.author),
            ).exists(),
            msg='Author should be created in the database',
        )

        self.assertTrue(
            Post.objects.filter(
                author__full_name=post_data.author,
                link=post_data.url,
            ).exists(),
            msg='Post should be created in the database',
        )

        self.assertEqual(
            WordCounter.objects.filter(post__link=post_data.url).count(),
            number_of_words_in_text,
            msg='All words from the text should be created in the database',
        )

    @patch.object(TeoniteBlogWebScraper, 'all_posts')
    def test_run(self, mocked_all_posts):
        number_of_words_in_text = 10
        first_post_data = PostData(
            author='John Doe',
            text=' '.join([generate_random_string() for _ in range(number_of_words_in_text)]),
            url=mommy.random_gen.gen_url(),
        )
        second_post_data = PostData(
            author='John Doe',
            text=' '.join([generate_random_string() for _ in range(number_of_words_in_text)]),
            url=mommy.random_gen.gen_url(),
        )

        mocked_all_posts.return_value = [first_post_data, second_post_data, ]
        self.importer.run()

        self.assert_post_imported_properly(first_post_data, number_of_words_in_text)
        self.assert_post_imported_properly(second_post_data, number_of_words_in_text)

    def test_import_post(self):
        number_of_words_in_text = 10
        post_data = PostData(
            author='John Doe',
            text=' '.join([generate_random_string() for _ in range(number_of_words_in_text)]),
            url=mommy.random_gen.gen_url(),
        )

        self.importer._import_post(
            post_data.author,
            post_data.text,
            post_data.url,
        )
        self.assert_post_imported_properly(post_data, number_of_words_in_text)

    def test_import_post_multiple_times(self):
        """
        Post should be imported only once.
        """
        number_of_words_in_text = 10
        post_data = PostData(
            author='John Doe',
            text=' '.join([generate_random_string() for _ in range(number_of_words_in_text)]),
            url=mommy.random_gen.gen_url(),
        )

        for _ in range(3):
            self.importer._import_post(
                post_data.author,
                post_data.text,
                post_data.url,
            )

        self.assertEqual(
            Author.objects.filter(
                full_name=post_data.author,
                username=Author.full_name_to_username(post_data.author),
            ).count(),
            1,
            msg='Author should be created in the database only once',
        )

        self.assertEqual(
            Post.objects.filter(
                author__full_name=post_data.author,
                link=post_data.url,
            ).count(),
            1,
            msg='Post should be created in the database only once',
        )

        self.assertEqual(
            WordCounter.objects.filter(post__link=post_data.url).count(),
            number_of_words_in_text,
            msg='All words from the text should be created in the database only once',
        )

    def test_import_words(self):
        post = mommy.make(Post)
        words_to_be_imported = [generate_random_string() for _ in range(10)]

        self.importer._import_words(post, words_to_be_imported)

        self.assertEqual(post.wordcounter_set.count(), len(words_to_be_imported))

        for word in words_to_be_imported:
            self.assertTrue(
                WordCounter.objects.filter(post=post, word=word, counter=1).exists(),
                msg='New word should be imported with counter equal to 1',
            )

    @parameterized.expand([
        ('', []),
        ('first line \n second line', ['first', 'line', 'second', 'line', ]),
        ('Sentence, where "sentence" occurs two times.', [
            'Sentence', 'where', 'sentence', 'occurs', 'two', 'times',
        ]),
        ('Weird,^%$.. punctuation', [
            'Weird', 'punctuation',
        ]),
        ('Numbers like 2020 are also stored', [
            'Numbers', 'like', '2020', 'are', 'also', 'stored',
        ]),
    ])
    def test_get_words(self, text, expected_words):
        words = self.importer._get_words(text)
        self.assertListEqual(words, expected_words)
