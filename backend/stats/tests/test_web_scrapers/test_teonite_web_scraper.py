from unittest import TestCase
from unittest.mock import MagicMock, patch

import requests
from model_mommy import mommy

from stats.web_scrapers import PageDoesNotExist, TeoniteBlogWebScraper


class TeoniteWebScraperTestCase(TestCase):
    # pylint: disable=invalid-name, protected-access

    def setUp(self):
        self.web_scraper = TeoniteBlogWebScraper()

    @patch.object(TeoniteBlogWebScraper, '_get_posts_links')
    @patch.object(TeoniteBlogWebScraper, '_get_post_data')
    def test_get_all_posts(self, mocked_get_post_data, mocked_get_posts_links):
        post_link = f'{self.web_scraper.BASE_URL}/blog/post'
        mocked_get_posts_links.return_value = [post_link, ]

        post_data = {
            'author': 'John Doe',
            'text': 'Post content',
            'url': post_link,
        }

        mocked_get_post_data.return_value = post_data

        all_posts = list(self.web_scraper.all_posts())

        expected_posts_data = [post_data, ]
        self.assertListEqual(all_posts, expected_posts_data)

    @patch.object(TeoniteBlogWebScraper, '_get_posts_links_from_page')
    def test_get_posts_links_for_multiple_pages(self, mocked_get_posts_links_from_page):
        mocked_get_posts_links_from_page.side_effect = [
            ['link1', 'link2'],  # 1st page
            ['link3', 'link4'],  # 2nd page
            PageDoesNotExist(),
        ]
        post_links = self.web_scraper._get_posts_links()
        self.assertListEqual(post_links, ['link1', 'link2', 'link3', 'link4', ])

    @patch.object(TeoniteBlogWebScraper, '_get_posts_links_from_page')
    def test_get_posts_links_when_page_is_not_found(self, mocked_get_posts_links_from_page):
        mocked_get_posts_links_from_page.side_effect = PageDoesNotExist()
        post_links = self.web_scraper._get_posts_links()
        self.assertListEqual(post_links, [])

    def test_get_posts_links_from_page(self):
        mocked_session = MagicMock()
        mocked_response = MagicMock()
        mocked_session.get.return_value = mocked_response

        mocked_response.status_code = requests.codes.ALL_OK  # pylint: disable=no-member
        relative_url = '/blog/the-post/'
        mocked_response.content = f'''
        <article>
            <a class="read-more" href={relative_url}>POST</a>
        </article>
        '''

        posts_links = self.web_scraper._get_posts_links_from_page(mocked_session, 1)

        mocked_session.get.assert_called_once()
        self.assertEqual(len(posts_links), 1)
        self.assertIn(self.web_scraper.BASE_URL + relative_url, posts_links)

    @patch('stats.web_scrapers.requests.get')
    def test_get_post_data(self, mocked_get):
        mocked_response = MagicMock()
        post_link = mommy.random_gen.gen_url()
        author_full_name = 'John Doe'
        post_content = 'This is post content.'
        mocked_response.content = f'''
        <section class="post-content"><p>{post_content}</p><img src="x.jpg" alt="alt"/></section>
        <span class="author-content"><h4>{author_full_name}</h4></span>
        '''
        mocked_get.return_value = mocked_response
        post_data = self.web_scraper._get_post_data(post_link)

        self.assertEqual(post_data.author, author_full_name)
        self.assertEqual(post_data.text, post_content)
        self.assertEqual(post_data.url, post_link)
