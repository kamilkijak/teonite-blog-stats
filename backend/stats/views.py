from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.views import APIView

from authors.models import Author
from stats.models import WordCounter


class MostPopularWordsView(APIView):
    http_method_names = ['get', ]

    NUMBER_OF_WORDS = 10

    def get(self, request):
        stats = WordCounter.get_most_popular_words(self.NUMBER_OF_WORDS)
        response_data = {item['word']: item['counter'] for item in stats}
        return Response(data=response_data)


class AuthorStatsView(APIView):
    http_method_names = ['get', ]

    NUMBER_OF_WORDS = 10

    def get(self, request, username):
        author = get_object_or_404(Author, username=username)
        stats = WordCounter.get_author_most_popular_words(author, self.NUMBER_OF_WORDS)
        response_data = {item['word']: item['counter'] for item in stats}
        return Response(data=response_data)
