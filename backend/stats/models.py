from django.db import models
from django.db.models import Sum

from authors.models import Author


class Post(models.Model):
    author = models.ForeignKey(Author, on_delete=models.CASCADE)
    link = models.URLField(unique=True)

    def __repr__(self):
        return self.link


class WordCounter(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    word = models.CharField(max_length=255, null=False, default=None)
    counter = models.PositiveSmallIntegerField()

    class Meta:
        unique_together = (('post', 'word'), )

    def __repr__(self):
        return f'{self.word} - {self.counter}'

    @classmethod
    def get_most_popular_words(cls, limit: int) -> models.query.QuerySet:
        return cls.objects \
                   .values('word') \
                   .annotate(counter=models.Sum('counter')) \
                   .order_by('-counter')[:limit]

    @classmethod
    def get_author_most_popular_words(cls, author: Author, limit: int) -> models.query.QuerySet:
        return WordCounter.objects \
                   .filter(post__author=author) \
                   .values('word') \
                   .annotate(counter=Sum('counter')) \
                   .order_by('-counter')[:limit]
