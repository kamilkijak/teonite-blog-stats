from django.urls import path

from stats.views import AuthorStatsView, MostPopularWordsView

urlpatterns = [
    path('', MostPopularWordsView.as_view(), name='most-popular-words'),
    path('<str:username>/', AuthorStatsView.as_view(), name='author-stats'),
]
