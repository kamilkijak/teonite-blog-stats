import logging
import re

from django.db import transaction
from django.db.models import F

from authors.models import Author
from stats.models import Post, WordCounter
from stats.web_scrapers import TeoniteBlogWebScraper

logger = logging.getLogger(__name__)  # pylint: disable=invalid-name


class TeoniteBlogPostsImporter:
    """
    Saves data from web scraper into the database.
    """

    def __init__(self):
        self.compiled_regex = re.compile(r'\w+')

    def run(self) -> None:
        """
        Fetches posts from the blog and saves them into the database.
        """
        web_scraper = TeoniteBlogWebScraper()

        for post_data in web_scraper.all_posts():
            logger.info('Importing post from %s', post_data.url)
            self._import_post(
                post_data.author,
                post_data.text,
                post_data.url,
            )

    def _import_post(self, author_full_name: str, post_text: str, post_url: str) -> None:
        """
        Save into the database:
            - author of the post
            - post itself
            - words from the post (with their counters)
        """
        with transaction.atomic():
            author, _ = Author.objects.get_or_create(
                username=Author.full_name_to_username(author_full_name),
                full_name=author_full_name,
            )

            post, post_created = Post.objects.get_or_create(
                link=post_url,
                defaults={
                    'author': author,
                },
            )

            if post_created:  # don't import words if post has been already imported
                post_words = self._get_words(post_text)
                self._import_words(post, post_words)
            else:
                logger.info('Skipped, post from %s already exists in the database', post_url)

    @staticmethod
    def _import_words(post: Post, post_words: list) -> None:
        for word in post_words:
            word_counter, _ = WordCounter.objects.get_or_create(
                post=post,
                word=word.lower(),
                defaults={
                    'counter': 0,
                },
            )
            word_counter.counter = F('counter') + 1
            word_counter.save()

    def _get_words(self, text: str) -> list:
        """
        Gets words from string.
        Eg. for 'one two three' -> ['one', 'two', 'three']
        """
        return self.compiled_regex.findall(text)
