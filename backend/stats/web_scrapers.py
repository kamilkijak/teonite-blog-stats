import typing
from collections import namedtuple

import requests
from bs4 import BeautifulSoup

PostData = namedtuple('PostData', ['author', 'text', 'url', ])


class PageDoesNotExist(Exception):
    """ Raised when page doesn't exist """


class TeoniteBlogWebScraper:  # TODO: consider using asyncio to improve performance
    """
    Web scraper for retrieving posts from Teonite blog.
    """

    BASE_URL = 'https://teonite.com'

    def __init__(self):
        self.blog_page_template = lambda page_number: f'{self.BASE_URL}/blog/page/{page_number}/'
        self.post_links = []
        self._post_index = 0

    def all_posts(self) -> typing.Generator[PostData, None, None]:
        """
        Generator for retrieving posts from the blog.
        """
        self._post_index = 0
        self.post_links = self._get_posts_links()

        while self._post_index < len(self.post_links):
            post_data = self._get_post_data(self.post_links[self._post_index])
            self._post_index += 1
            yield post_data

    def _get_posts_links(self) -> list:
        """
        Scrapes links to posts from the blog, page by page.
        """
        page_number = 1
        all_links = []

        with requests.Session() as session:
            try:
                while True:
                    all_links += self._get_posts_links_from_page(session, page_number)
                    page_number += 1
            except PageDoesNotExist:
                return all_links

    def _get_posts_links_from_page(self, session: requests.Session, page_number: int) -> list:
        """
        Scrapes links to posts from specific page.
        """
        blog_page = session.get(self.blog_page_template(page_number))

        if blog_page.status_code != requests.status_codes.codes.ALL_OK:  # pylint: disable=no-member
            raise PageDoesNotExist

        soup = BeautifulSoup(blog_page.content, 'html.parser')
        posts = soup.findAll('article')
        posts_links = [
            self.BASE_URL + post.find('a', {'class': 'read-more'}).get('href')
            for post in posts
        ]
        return posts_links

    @staticmethod
    def _get_post_data(post_link: str) -> PostData:
        """
        Scrapes post from specific url.
        """
        post_page = requests.get(post_link)
        soup = BeautifulSoup(post_page.content, 'html.parser')
        post_content = soup.find('section', {'class': 'post-content'})
        post_text = post_content.text.replace('\n', ' ')
        author = soup.select_one('span.author-content > h4').text
        return PostData(author=author, text=post_text, url=post_link)
