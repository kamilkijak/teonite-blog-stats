coverage==4.5.1
isort==4.3.4
model_mommy==1.6.0
parameterized==0.6.1
pylint==2.0.1
pylint-django==2.0